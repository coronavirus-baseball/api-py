import os
import json
import boto3

def dynamo_to_record(item):
    return {
        "id": item.get('video_id', {}).get('S', None),
        "title": item.get('title', {}).get('S', None),
        "description": item.get('description', {}).get('S', None),
        "thumbnail": item.get('thumbnail', {}).get('S', None),
        "duration_seconds": item.get('duration_seconds', {}).get('N', None),
        "team": item.get('team', {}).get('S', None),
        "season": item.get('season', {}).get('S', None),
        "created_at": item.get('created_at', {}).get('S', None)
    }

def lambda_handler(event, context):
    client = boto3.client('dynamodb')

    team = event["queryStringParameters"].get('team', None)
    season = event["queryStringParameters"].get('season', None)

    if not team and not season:
        # primary key scan
        results = client.scan(TableName=os.environ['DYNAMODB_TABLE'])
    elif team and not season:
        # team index scan
        results = client.scan(TableName=os.environ['DYNAMODB_TABLE'], IndexName='team_index',
            ScanFilter={
                'team': {
                    'AttributeValueList': [
                        {
                            'S': team
                        }
                    ],
                    'ComparisonOperator': 'EQ'
                }
            }
        )
    elif season and not team:
        # season index scan
        results = client.scan(TableName=os.environ['DYNAMODB_TABLE'], IndexName='season_index',
            ScanFilter={
                'season': {
                    'AttributeValueList': [
                        {
                            'S': season
                        }
                    ],
                    'ComparisonOperator': 'EQ'
                }
            }
        )
    else:
        # query
        results = client.query(TableName=os.environ['DYNAMODB_TABLE'],IndexName='team-season-index', 
            KeyConditions={
                'team': {
                    'AttributeValueList': [
                        {
                            'S': team
                        }
                    ],
                    'ComparisonOperator': 'EQ'
                },
                'season': {'AttributeValueList': [
                        {
                            'S': season
                        }
                    ],
                    'ComparisonOperator': 'EQ'
                }
            }
        )
    records = [dynamo_to_record(r) for r in results.get('Items', [])]

    return {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "https://www.coronavirus-baseball.com",
        },
        "body": json.dumps({
            "videos": records,
            "parameters": {
                "team": team,
                "season": season
            }
        })
    }
